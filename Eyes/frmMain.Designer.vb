﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.timFollow = New System.Windows.Forms.Timer(Me.components)
        Me.picLeft = New System.Windows.Forms.PictureBox()
        Me.picRight = New System.Windows.Forms.PictureBox()
        CType(Me.picLeft, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'timFollow
        '
        Me.timFollow.Enabled = True
        Me.timFollow.Interval = 50
        '
        'picLeft
        '
        Me.picLeft.BackColor = System.Drawing.Color.Transparent
        Me.picLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picLeft.Image = Global.Eyes.My.Resources.Resources.eyes_front_t
        Me.picLeft.InitialImage = Global.Eyes.My.Resources.Resources.eyes_front_t
        Me.picLeft.Location = New System.Drawing.Point(78, 78)
        Me.picLeft.Name = "picLeft"
        Me.picLeft.Size = New System.Drawing.Size(50, 50)
        Me.picLeft.TabIndex = 2
        Me.picLeft.TabStop = False
        '
        'picRight
        '
        Me.picRight.BackColor = System.Drawing.Color.Transparent
        Me.picRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.picRight.Image = Global.Eyes.My.Resources.Resources.eyes_front_t
        Me.picRight.InitialImage = Global.Eyes.My.Resources.Resources.eyes_front_t
        Me.picRight.Location = New System.Drawing.Point(276, 78)
        Me.picRight.Name = "picRight"
        Me.picRight.Size = New System.Drawing.Size(50, 50)
        Me.picRight.TabIndex = 3
        Me.picRight.TabStop = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.BackgroundImage = Global.Eyes.My.Resources.Resources.eyes_back_t
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(400, 210)
        Me.Controls.Add(Me.picRight)
        Me.Controls.Add(Me.picLeft)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Eyes"
        Me.TopMost = True
        Me.TransparencyKey = System.Drawing.SystemColors.ControlLightLight
        CType(Me.picLeft, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents timFollow As System.Windows.Forms.Timer
    Friend WithEvents picLeft As System.Windows.Forms.PictureBox
    Friend WithEvents picRight As System.Windows.Forms.PictureBox

End Class
