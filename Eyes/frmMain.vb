﻿Public Class frmMain

    Private Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer
    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.TransparencyKey = Me.BackColor
        Me.Icon = My.Resources.eyes
    End Sub

    Private Sub timFollow_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timFollow.Tick
        Dim iMX As Integer = Windows.Forms.Cursor.Position.X
        Dim iMY As Integer = Windows.Forms.Cursor.Position.Y
        Dim iLX As Integer = Me.DesktopBounds.Left + iCenterLeftX
        Dim iLY As Integer = Me.DesktopBounds.Top + iCenterLeftY
        Dim iRX As Integer = Me.DesktopBounds.Left + iCenterRightX
        Dim iRY As Integer = Me.DesktopBounds.Top + iCenterRightY
        Dim dWL As Double = Math.Atan2(iMY - iLY, iMX - iLX)
        Dim dWR As Double = Math.Atan2(iMY - iRY, iMX - iRX)
        Dim iRadLeft As Integer = iRadius, iRadRight As Integer = iRadius
        Dim iTrackLeft As Integer = Math.Sqrt((iMY - iLY) ^ 2 + (iMX - iLX) ^ 2)
        If iTrackLeft < iRadLeft Then iRadLeft = iTrackLeft
        Dim iTrackRight As Integer = Math.Sqrt((iMY - iRY) ^ 2 + (iMX - iRX) ^ 2)
        If iTrackRight < iRadRight Then iRadRight = iTrackRight
        Dim iELX As Integer = iCenterLeftX + iRadLeft * Math.Cos(dWL) - iRadiusIris
        Dim iELY As Integer = iCenterLeftY + iRadLeft * Math.Sin(dWL) - iRadiusIris
        Dim iERX As Integer = iCenterRightX + iRadRight * Math.Cos(dWR) - iRadiusIris
        Dim iERY As Integer = iCenterRightY + iRadRight * Math.Sin(dWR) - iRadiusIris
        picLeft.Left = iELX
        picLeft.Top = iELY
        picRight.Left = iERX
        picRight.Top = iERY
        Application.DoEvents()
    End Sub

    Private Sub frmMain_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles picLeft.MouseClick, picRight.Click
        If e.Button = Windows.Forms.MouseButtons.Right Then
            Dim Result = MsgBox("Eyes beenden?", MsgBoxStyle.YesNo)
            If Result = MsgBoxResult.Yes Then timFollow.Stop() : Me.Close() : Application.Exit()
        End If
    End Sub

    Private Sub frmMain_MouseClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picRight.Click, picLeft.MouseClick

    End Sub
End Class
